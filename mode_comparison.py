#!/usr/bin/env python3

from debt_payoff import DebtPayoff
import numpy as np

thousands_separator = DebtPayoff.thousands_separator


class CompareScenarios:
    def __init__(self,
                 ira_participation=False):
        self.results = dict()
        self.interest = dict()
        self.payments = dict()
        self.ira_participation = ira_participation
        self.retirement = dict()
        alpha = ['H', 'I', 'L']
        beta = ['H', 'I', 'T', 'L']
        combos = []
        for a in alpha:
            for b in beta:
                for c in alpha:
                    combos.append(a + b + c)
        for x in combos:
            self.results[x] = list()
            self.interest[x] = list()
            self.payments[x] = list()
            self.retirement[x] = list()

    def payment_comparison(self, debug=False, ira_participation=False):
        # interest driven
        interest_driven = DebtPayoff(ira_participation=ira_participation)
        for k in range(1, 10):
            interest_driven.random_loan()
        loans = interest_driven.copy_dictionary(interest_driven.loans)
        interest_driven.interest_driven()
        if debug:
            print("Interest driven: $%s, %s payments" % (thousands_separator(interest_driven.interest_paid),
                                                         interest_driven.payments))
            if ira_participation:
                print("Net worth: $%s" % thousands_separator(interest_driven.retirement))
                print("Net worth per month: $%s" % thousands_separator(interest_driven.retirement /
                                                                       interest_driven.payments))

        # lowest balance driven
        lowest_balance = DebtPayoff(ira_participation=ira_participation)
        lowest_balance.loans = lowest_balance.copy_dictionary(loans)
        lowest_balance.low_balance_driven()
        if debug:
            print('')
            print("Lowest Balance Driven: $%s, %s payments" % (thousands_separator(lowest_balance.interest_paid),
                                                               lowest_balance.payments))
            if ira_participation:
                print("Net worth: $%s" % thousands_separator(lowest_balance.retirement))
                print("Net worth per month: $%s" % thousands_separator(
                    lowest_balance.retirement / lowest_balance.payments))

        # maximum balance driven
        highest_balance = DebtPayoff(ira_participation=ira_participation)
        highest_balance.loans = highest_balance.copy_dictionary(loans)
        highest_balance.high_balance_driven()
        if debug:
            print('')
            print("Highest balance Driven: $%s, %s payments" % (thousands_separator(
                highest_balance.interest_paid),
                                                                highest_balance.payments))
            if ira_participation:
                print("Net worth: $%s" % thousands_separator(highest_balance.retirement))
                print("Net worth per month: $%s" % thousands_separator(highest_balance.retirement /
                                                                       highest_balance.payments))

        winning_strategy = ''
        # Which plans require you to pay the least interest?
        if highest_balance.interest_paid < interest_driven.interest_paid:
            if highest_balance.interest_paid < lowest_balance.interest_paid:
                winning_strategy += 'H'
            else:
                winning_strategy += 'L'
        else:
            if interest_driven.interest_paid < lowest_balance.interest_paid:
                winning_strategy += 'I'
            else:
                winning_strategy += 'L'

        # which plans have the shortest payoff time?
        if highest_balance.payments < interest_driven.payments:
            if highest_balance.payments < lowest_balance.payments:
                winning_strategy += 'H'
            else:
                winning_strategy += 'L'
        elif lowest_balance.payments == interest_driven.payments == highest_balance.payments:
            winning_strategy += 'T'
        else:
            if interest_driven.payments < lowest_balance.payments:
                winning_strategy += 'I'
            else:
                winning_strategy += 'L'

        # Which plans have the highest net worth gain per month?
        if highest_balance.retirement / highest_balance.payments > interest_driven.retirement / \
                interest_driven.payments:
            if highest_balance.retirement / highest_balance.payments > lowest_balance.retirement / \
                    lowest_balance.payments:
                winning_strategy += 'H'
            else:
                winning_strategy += 'L'
        else:
            if interest_driven.retirement / interest_driven.payments > lowest_balance.retirement / \
                    lowest_balance.payments:
                winning_strategy += "I"
            else:
                winning_strategy += 'L'

        # print(loans)
        # print(winning_strategy)
        self.results[winning_strategy].append(loans)
        self.interest[winning_strategy].append(np.std([interest_driven.interest_paid, highest_balance.interest_paid,
                                                       lowest_balance.interest_paid]) -
                                               min([interest_driven.interest_paid, highest_balance.interest_paid,
                                                    lowest_balance.interest_paid]))
        self.payments[winning_strategy].append(np.std([interest_driven.payments, highest_balance.payments,
                                                       lowest_balance.payments]) -
                                               min([interest_driven.payments, highest_balance.payments,
                                                    lowest_balance.payments]))
        self.retirement[winning_strategy].append(max([interest_driven.retirement, highest_balance.retirement,
                                                      lowest_balance.payments]) -
                                                 np.std([interest_driven.retirement, highest_balance.retirement,
                                                         lowest_balance.payments]))


if __name__ == "__main__":
    scenarios = CompareScenarios(ira_participation=True)
    for iteration in range(10 ** 4):
        scenarios.payment_comparison()
        if iteration % 1000 == 0:
            print(iteration)

    print("Interest, payments, retirement")

    print('results')
    for key, value in scenarios.results.items():
        if len(value):
            print(key, len(value))

    print('\n')
    print('interest saved')
    for key, value in scenarios.interest.items():
        if scenarios.results[key]:
            print(key, thousands_separator(sum(value)),
                  thousands_separator(sum(value) / len(scenarios.results[key])),
                  np.std(value)
                  )

    print('\n')
    print('payments saved')
    for key, value in scenarios.payments.items():
        if scenarios.results[key]:
            print(key, sum(value),
                  thousands_separator(sum(value) / len(scenarios.results[key])),
                  np.std(value)
                  )

    print('\n')
    print('Net worth')
    for key, value in scenarios.retirement.items():
        if sum(value):
            print(key, sum(value), np.std(value))
    # print(scenarios.results)
