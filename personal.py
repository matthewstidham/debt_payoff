#!/usr/bin/env python3

from debt_payoff import DebtPayoff
import argparse

class Personal(DebtPayoff):
    def add_personal(self):
        check = True
        while check:
            loan = input('loan name ')
            if not loan:
                break
            amount = input('amount ')
            interest = input('interest ')
            self.loans[loan] = {'interest': float(interest),
                                'balance': float(amount)}
            print('Leave blank if no more loans')
        print(self.loans)

    def set_data(self):
        self.income = input('Income: ')
        self.taxes = input('Tax rate: ')
        self.housing = input('Housing cost:')
        self.ira_percentage = input("IRA contribution")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--ira', action='store_true')
    args = parser.parse_args()

    interest_driven = Personal(debug=args.debug, ira_participation=args.ira)
    interest_driven.add_personal()
    loans = interest_driven.copy_dictionary(interest_driven.loans)
    interest_driven.set_data()
    ira_participation = interest_driven.ira_participation

    interest_driven.interest_driven()
    print("Interest driven: $%s, %s payments" % (interest_driven.thousands_separator(interest_driven.interest_paid),
                                                 interest_driven.payments))
    if ira_participation:
        print("Net worth: $%s" % interest_driven.thousands_separator(interest_driven.retirement))
        print("Net worth per month: $%s" % interest_driven.thousands_separator(interest_driven.retirement /
                                                                               interest_driven.payments))

    if args.debug:
        print('\n')
        print('-' * 30)
        print('\n')
    highest_balance = Personal(debug=args.debug, ira_participation=args.ira)
    highest_balance.loans = loans
    print('highest balance loans: %s' % loans)
    highest_balance.income = interest_driven.income
    highest_balance.taxes = interest_driven.taxes
    highest_balance.housing = interest_driven.housing
    highest_balance.ira_participation = ira_participation

    print("Highest balance: $%s, %s payments" % (highest_balance.thousands_separator(highest_balance.interest_paid),
                                                 highest_balance.payments))
    highest_balance.high_balance_driven()

    print("Highest balance: $%s, %s payments" % (highest_balance.thousands_separator(highest_balance.interest_paid),
                                                 highest_balance.payments))
    if ira_participation:
        print("Net worth: %s" % highest_balance.thousands_separator(highest_balance.retirement))
        print("Net worth per month: $%s" % highest_balance.thousands_separator(highest_balance.retirement /
                                                                               highest_balance.payments))
