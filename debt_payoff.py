#!/usr/bin/env python3
import random
import string


class DebtPayoff:
    def __init__(self, debug=False, matching_ira=False, ira_participation=False, ira_percentage=.25):
        self.loans = dict()
        self.payments = 0
        self.interest_paid = 0
        self.income = 50000
        self.taxes = 0.2
        self.housing = 1000
        self.disposable = self.income * (1 - self.taxes) / 12 - self.housing
        self.debug = debug
        self.retirement = 0
        self.matching_ira = matching_ira
        self.ira_participation = ira_participation
        self.ira_percentage = ira_percentage

    def add_loan(self, name, interest_rate, balance):
        self.loans[name] = {'interest': interest_rate,
                            'balance': balance}

    def random_loan(self):
        loan = ''.join(random.choices(string.ascii_lowercase, k=5))
        self.add_loan(name=loan,
                      interest_rate=random.randint(1, 30) / 1000,
                      balance=random.randint(1, 10) * 1000
                      )

    def highest_interest(self):
        highest_interest = 0
        loan = None
        for key, value in self.loans.items():
            interest = value['interest']
            if interest > highest_interest:
                loan = key
                highest_interest = interest
        return loan

    def highest_balance(self):
        highest_balance = 0
        loan = None
        for key, value in self.loans.items():
            balance = value['balance']
            if value['interest'] > highest_balance:
                loan = key
                highest_balance = balance
        return loan

    def lowest_balance(self):
        lowest_balance = 10 ** 10
        loan = None
        for key, value in self.loans.items():
            balance = value['balance']
            if value['interest'] < lowest_balance:
                loan = key
                lowest_balance = balance
        return loan

    def payoff_interest(self, payment):
        for loan, value in self.loans.items():
            if payment > 0:
                interest_payment = round(self.loans[loan]['balance'] * self.loans[loan]['interest'] ** (1 / 12), 2)
                if self.debug:
                    print(loan, interest_payment)
                payment -= interest_payment
                self.interest_paid += interest_payment
                self.loans[loan]['balance'] -= interest_payment
                self.loans[loan]['balance'] = round(self.loans[loan]['balance'], 2)
            return payment

    def interest_driven(self):
        while sum([x['balance'] for x in self.loans.values()]) > 0:
            payment = self.disposable / 2
            highest = self.highest_interest()

            if self.ira_participation:
                payment *= 1 - self.ira_percentage
                self.retirement += self.disposable / 2 * self.ira_percentage * (1 + self.matching_ira)

            if self.debug:
                print(self.loans)
                print('Interest time')
            # payoff interest on each loan
            payment = self.payoff_interest(payment)

            # then pay down highest interest loan
            if self.debug:
                print(self.loans)
                print(payment)
            while payment > 0 and highest:
                if self.debug:
                    print("Month: %s, Loan: %s, Payment: %s" % (self.payments,
                                                                highest,
                                                                payment))
                if self.loans[highest]['balance'] > payment:
                    self.loans[highest]['balance'] -= payment
                    payment = 0
                else:
                    payment -= self.loans[highest]['balance']
                    self.loans.pop(highest)
                    highest = self.highest_interest()

            self.payments += 1
            self.retirement *= 1.08 ** (1 / 12)
            if self.debug:
                print('Payment: %s' % self.payments)
                print('Loans: %s' % self.loans)
                print('\n')

    def high_balance_driven(self):
        while sum([x['balance'] for x in self.loans.values()]) > 0:
            payment = self.disposable / 2
            highest = self.highest_balance()

            if self.ira_participation:
                payment *= 1 - self.ira_percentage
                self.retirement += self.disposable / 2 * self.ira_percentage * (1 + self.matching_ira)

            # payoff interest on each loan
            payment = self.payoff_interest(payment)

            # then pay down highest balance
            while payment > 0 and highest:
                if self.loans[highest]['balance'] > payment:
                    self.loans[highest]['balance'] -= payment
                    payment = 0
                else:
                    payment -= self.loans[highest]['balance']
                    self.loans.pop(highest)
                    highest = self.highest_balance()

            self.payments += 1
            self.retirement *= 1.08 ** (1 / 12)
            if self.debug:
                print(self.payments)
                print(sum([x['balance'] for x in self.loans.values()]))

    def low_balance_driven(self):
        while sum([x['balance'] for x in self.loans.values()]) > 0:
            payment = self.disposable / 2
            lowest = self.lowest_balance()

            if self.ira_participation:
                payment *= 1 - self.ira_percentage
                self.retirement += self.disposable / 2 * self.ira_percentage * (1 + self.matching_ira)

            # payoff interest on each loan
            payment = self.payoff_interest(payment)

            # then pay down highest balance
            while payment > 0 and lowest:
                if self.loans[lowest]['balance'] > payment:
                    self.loans[lowest]['balance'] -= payment
                    payment = 0
                else:
                    payment -= self.loans[lowest]['balance']
                    self.loans.pop(lowest)
                    lowest = self.lowest_balance()

            self.payments += 1
            self.retirement *= 1.08 ** (1 / 12)
            if self.debug:
                print(self.payments)
                print(sum([x['balance'] for x in self.loans.values()]))

    @staticmethod
    def copy_dictionary(dictionary):
        loans = dict()
        for method, test in dictionary.items():
            loan = dict()
            for key_2, value_2, in test.items():
                loan[key_2] = value_2
            loans[method] = loan
        return loans

    @staticmethod
    def thousands_separator(value):
        value = str(round(value, 2))
        if '.' in value:
            thousands, decimal = value.split('.')
        else:
            thousands = value
            decimal = ''

        if len(thousands) < 3:
            if decimal:
                return '.'.join([thousands, decimal]).strip('.')
            else:
                return thousands

        remainder = len(thousands) % 3
        if remainder:
            thousands = thousands[remainder:]
            beginning = value[:remainder]
        else:
            beginning = ''
        thousands = ','.join([thousands[start:start + 3] for start in range(0, len(thousands), 3)])
        if beginning:
            thousands = ','.join([beginning, thousands])
        return '.'.join([thousands, decimal]).strip('.')


def payment_comparison(debug=False, ira_participation=False):
    # interest driven
    interest_driven = DebtPayoff(ira_participation=ira_participation)
    for k in range(1, 10):
        interest_driven.random_loan()
    loans = interest_driven.copy_dictionary(interest_driven.loans)
    interest_driven.interest_driven()
    if debug:
        print("Interest driven: $%s, %s payments" % (DebtPayoff.thousands_separator(interest_driven.interest_paid),
                                                     interest_driven.payments))
        if ira_participation:
            print("Net worth: $%s" % DebtPayoff.thousands_separator(interest_driven.retirement))
            print("Net worth per month: $%s" % DebtPayoff.thousands_separator(interest_driven.retirement /
                                                                              interest_driven.payments))

    # lowest balance driven
    lowest_balance = DebtPayoff(ira_participation=ira_participation)
    lowest_balance.loans = lowest_balance.copy_dictionary(loans)
    lowest_balance.low_balance_driven()
    if debug:
        print('')
        print("Lowest Balance Driven: $%s, %s payments" % (DebtPayoff.thousands_separator(lowest_balance.interest_paid),
                                                           lowest_balance.payments))
        if ira_participation:
            print("Net worth: $%s" % DebtPayoff.thousands_separator(lowest_balance.retirement))
            print("Net worth per month: $%s" % DebtPayoff.thousands_separator(lowest_balance.retirement /
                                                                              lowest_balance.payments))

    # maximum balance driven
    highest_balance = DebtPayoff(ira_participation=ira_participation)
    highest_balance.loans = highest_balance.copy_dictionary(loans)
    highest_balance.high_balance_driven()
    if debug:
        print('')
        print("Highest balance Driven: $%s, %s payments" % (DebtPayoff.thousands_separator(
            highest_balance.interest_paid),
                                                            highest_balance.payments))
        if ira_participation:
            print("Net worth: $%s" % DebtPayoff.thousands_separator(highest_balance.retirement))
            print("Net worth per month: $%s" % DebtPayoff.thousands_separator(highest_balance.retirement /
                                                                              highest_balance.payments))

    winning_strategy = ''
    # Which plans require you to pay the least interest?
    if highest_balance.interest_paid < interest_driven.interest_paid:
        if highest_balance.interest_paid < lowest_balance.interest_paid:
            winning_strategy += 'H'
        else:
            winning_strategy += 'L'
    else:
        if interest_driven.interest_paid < lowest_balance.interest_paid:
            winning_strategy += 'I'
        else:
            winning_strategy += 'L'

    # which plans have the shortest payoff time?
    if highest_balance.payments < interest_driven.payments:
        if highest_balance.payments < lowest_balance.payments:
            winning_strategy += 'H'
        else:
            winning_strategy += 'L'
    elif lowest_balance.payments == interest_driven.payments == highest_balance.payments:
        winning_strategy += 'T'
    else:
        if interest_driven.payments < lowest_balance.payments:
            winning_strategy += 'I'
        else:
            winning_strategy += 'L'

    # Which plans have the highest net worth gain per month?
    if highest_balance.retirement / highest_balance.payments > interest_driven.retirement / interest_driven.payments:
        if highest_balance.retirement / highest_balance.payments > lowest_balance.retirement / lowest_balance.payments:
            winning_strategy += 'H'
        else:
            winning_strategy += 'L'
    else:
        if interest_driven.retirement / interest_driven.payments > lowest_balance.retirement / lowest_balance.payments:
            winning_strategy += "I"
        else:
            winning_strategy += 'L'

    return winning_strategy


if __name__ == "__main__":
    results = dict()
    for count in range(10 ** 5):
        result = payment_comparison(ira_participation=True)
        if result in results:
            results[result] += 1
        else:
            results[result] = 1
        if count % 1000 == 0:
            print(count)
    print('least interest paid, fastest payoff time, highest net worth')
    print(' ')
    print(results)
    # print(payment_comparison(debug=True,
    #                          ira_participation=True))
